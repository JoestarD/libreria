<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->name = "Jonathan";
        $user->lastname = "Soto";
        $user->email="jsoto@uabcs.mx";
        $user->address ="calle del perrito 123";
        $user->phone = "61200000";
        $user->role = 1;
        $user->password = bcrypt("123");
        $user->save();

        $user = new User;
        $user->name = "Jonathan";
        $user->lastname = "Soto";
        $user->email="jsoto1429@gmail.mx";
        $user->address ="calle del perrito 123";
        $user->phone = "61200000";
        $user->role = 2;
        $user->password = bcrypt("123");
        $user->save();
    }
}
