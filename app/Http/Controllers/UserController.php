<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {	
    	$users = User::where('status','active')->get();

        return view('users.index',compact('users'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request)
    {       

        $request['password'] = bcrypt($request['password']);

         if ( User::create($request->all())) {
            
            return redirect()->back()->with('success', 'registro creado correctamente');

         }

         return redirect()->back()->with('error', 'elregistro  no pudo completarse');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update(Request $request)
    {       

        $user = null;

        if ($user = User::find($request['id'])) {

            if ($request['password']!="") {
                $request['password'] = bcrypt($request['password']);
            }else{
                $request['password'] = $user->password;
            }
            
        
             if (  $user->update($request->all()) ) {
                
                return redirect()->back()->with('success', 'registro creado correctamente');

             }

        }


       
        return redirect()->back()->with('error', 'elregistro  no pudo completarse');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show($id)
    {   
        $user = User::find($id);
        return $user;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function destroy($id)
    {   
        if($user = User::find($id)){

            $user->status="inactive";
            if ($user->save()) {

                return  response()->json([
                    'message' => 'Se ha eliminado el registro',
                    'code' => 2,
                    'data' => null
                ], 200);
            }
        }

        return  response()->json([
            'message' => 'No se puso eliminar el registro',
            'code' => -2,
            'data' => null
        ], 200);
    }
}
