@extends('layouts.app')

@section('head')
 <link href="{{ asset('app_assets/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('app_assets/vendor/sweetalert/sweetalert.css') }}">
@endsection

@section('content')

 <!-- DataTales Example -->
<div class="card shadow mb-4">
<div class="card-header py-3">
	<div class="row">
		<div class="col">
			<h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
		</div>
		<div class="col">
			<a href="#" data-toggle="modal" data-target="#addUser" class="btn btn-info btn-icon-split float-right">
	            <span class="icon text-white-50">
	              <i class="fas fa-info-circle"></i>
	            </span>
	            <span class="text">Add user</span>
	          </a>
		</div>
	</div>
  
</div>
<div class="card-body">
  <div class="table-responsive">
    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
      <thead>
        <tr>
          <th>Name</th>
          <th>Lastname</th>
          <th>Address</th>
          <th>Phone</th>
          <th>Email</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <th>Name</th>
          <th>Lastname</th>
          <th>Address</th>
          <th>Phone</th>
          <th>Email</th>
          <th>Actions</th>
        </tr>
      </tfoot>
      <tbody>
      	@if(isset($users) && count($users)>0)
      	@foreach($users as $user)
        <tr>
          <td>{{ $user->name }}</td>
          <td>{{ $user->lastname }}</td>
          <td>{{ $user->address }}</td>
          <td>{{ $user->phone }}</td>
          <td>{{ $user->email }}</td>
          <td>
          	<a href="#" onclick="Edit({{ $user->id	 }})" data-toggle="modal" data-target="#editUser" class="btn btn-warning btn-circle">
                <i class="fas fa-exclamation-triangle"></i>
            </a>
            <a href="#" onclick="Delete({{ $user->id }},this)" class="btn btn-danger btn-circle">
                <i class="fas fa-trash"></i>
            </a>
          </td>
        </tr> 
        @endforeach
        @endif
      </tbody>
    </table>
  </div>
</div>
</div>

@endsection

@section('modals')
 <!-- Logout Modal-->
  <div class="modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title text-center"  id="exampleModalLabel">
          	Add new user
          </h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <form method="POST" action="{{ URL::to('user') }}">
        <div class="modal-body">
        	@csrf
        	<div class="form-group">
			    <label for="exampleInputEmail1">Email address</label>
			    <input required="" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="email"> 
			</div>

			<div class="form-group">
			    <label for="exampleInputEmail1">
			    	Name
			    </label>
			    <input required="" type="text" class="form-control" id="name" aria-describedby="emailHelp" placeholder="Enter name" name="name"> 
			</div>

			<div class="form-group">
			    <label for="exampleInputEmail1">
			    	Lastname
			    </label>
			    <input required="" type="text" class="form-control" id="lastname" aria-describedby="emailHelp" placeholder="Enter lastname" name="lastname"> 
			</div>

			<div class="form-group">
			    <label for="exampleInputEmail1">
			    	Password
			    </label>
			    <input required="" type="password" class="form-control" id="password" aria-describedby="emailHelp" placeholder="Enter password" name="password"> 
			</div>

			<div class="form-group">
			    <label for="exampleInputEmail1">
			    	Addres
			    </label>
			    <input type="text" class="form-control" id="address" required="" aria-describedby="emailHelp" placeholder="Enter address" name="address"> 
			</div>

			<div class="form-group">
			    <label for="exampleInputEmail1">
			    	Phone
			    </label>
			    <input type="text" required="" class="form-control" id="phone" aria-describedby="emailHelp" placeholder="Enter phone" name="phone"> 
			</div> 


    	</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>

          <button class="btn btn-primary" type="submit" >Save</button>
        </div>
    	</form>

      </div>
    </div>
  </div>

  <div class="modal fade" id="editUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title text-center"  id="exampleModalLabel">
          	Edit new user
          </h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <form method="POST" action="{{ URL::to('user') }}">
        <div class="modal-body">
        	@csrf
        	<input type="hidden" name="id" id="id_edit">
        	<input name="_method" type="hidden" value="PUT">

        	<div class="form-group">
			    <label for="exampleInputEmail1">Email address</label>
			    <input required="" type="email" class="form-control" id="email_edit" aria-describedby="emailHelp" placeholder="Enter email" name="email"> 
			</div>

			<div class="form-group">
			    <label for="exampleInputEmail1">
			    	Name
			    </label>
			    <input required="" type="text" class="form-control" id="name_edit" aria-describedby="emailHelp" placeholder="Enter name" name="name"> 
			</div>

			<div class="form-group">
			    <label for="exampleInputEmail1">
			    	Lastname
			    </label>
			    <input required="" type="text" class="form-control" id="lastname_edit" aria-describedby="emailHelp" placeholder="Enter lastname" name="lastname"> 
			</div>

			<div class="form-group">
			    <label for="exampleInputEmail1">
			    	Password
			    </label>
			    <input required="" type="password" class="form-control" id="password_edit" aria-describedby="emailHelp" placeholder="Enter password" name="password"> 
			</div>

			<div class="form-group">
			    <label for="exampleInputEmail1">
			    	Addres
			    </label>
			    <input type="text" class="form-control" id="address_edit" required="" aria-describedby="emailHelp" placeholder="Enter address" name="address"> 
			</div>

			<div class="form-group">
			    <label for="exampleInputEmail1">
			    	Phone
			    </label>
			    <input type="text" required="" class="form-control" id="phone_edit" aria-describedby="emailHelp" placeholder="Enter phone" name="phone"> 
			</div> 


    	</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>

          <button class="btn btn-primary" type="submit" >Save</button>
        </div>
    	</form>

      </div>
    </div>
  </div>

@endsection


@section('scripts')
<!-- Page level plugins -->
  <script src="{{ asset('app_assets/vendor/datatables/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('app_assets/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

  <script type="text/javascript" src="{{ asset('app_assets/vendor/sweetalert/sweetalert.min.js') }}"></script>

  <!-- Page level custom scripts -->
  <script src="{{ asset('app_assets/js/demo/datatables-demo.js') }}"></script>
	<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
  <script type="text/javascript">
  	function Edit(id){

  		axios.get('{{ URL::to("user") }}/'+id)
		  .then(function (response) {
		     
		  	var data = response.data;

		  	$("#id_edit").val(data.id)
		  	$("#email_edit").val(data.email)
		  	$("#name_edit").val(data.name)
		  	$("#lastname_edit").val(data.lastname)
		  	/*$("#password_edit").val(data.id)*/
		  	$("#address_edit").val(data.address)
		  	$("#phone_edit").val(data.phone)

		  }); 

  	}

  	function Delete(id,element){

  		swal({
		  title: "Are you sure?",
		  text: "You will not be able to recover this imaginary file!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, delete it!",
		  cancelButtonText: "No, cancel plx!",
		  closeOnConfirm: false,
		  closeOnCancel: false
		},
		function(isConfirm) {
		  if (isConfirm) {

		  	axios.delete('{{ URL::to("user") }}/'+id)
		  .then(function (response) {
		     
		  	var data = response.data;

		  	 console.log(data)

		  	 if (data.code>0) {
		  	 	swal("Deleted!", "Your imaginary file has been deleted.", "success");

		  	 	$(element).parent().parent().remove();

		  	 }else{
		  	 	 swal("Cancelled", "Your imaginary file is safe :)", "error");
		  	 }

		  }); 

		    
		  } else {
		    swal("Cancelled", "Your imaginary file is safe :)", "error");
		  }
		});

  	}
  </script>
@endsection